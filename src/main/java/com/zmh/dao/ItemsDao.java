package com.zmh.dao;


import com.zmh.pojo.Items;

/**
 * @author Administrator
 * @title: ItemsMapper
 * @projectName maven_day02_1ssm
 * @description: TODO
 * @date 2019/5/520:54
 */
public interface ItemsDao {
    Items findById(Integer id);
}
