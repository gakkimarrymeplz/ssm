package com.zmh.controller;

import com.zmh.pojo.Items;
import com.zmh.service.ItemsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Administrator
 * @title: ItemsController
 * @projectName maven_day02_1ssm
 * @description: TODO
 * @date 2019/5/69:59
 */
@Controller
@RequestMapping("/items")
public class ItemsController {

    @Autowired
    private ItemsService itemsService;

    public String findDetail(Model model) {
        Items items = itemsService.findById(1);
        model.addAttribute("item","items");
        return "itemDetail";
    }

}
