package com.zmh.pojo;

import lombok.Data;

import java.util.Date;

/**
 * @author Administrator
 * @title: items
 * @projectName maven_day02_1ssm
 * @description: TODO
 * @date 2019/5/520:49
 */
@Data
public class Items {
    private Integer id;
    private String name;
    private Float price;
    private String pic;

    private Date createtime;
    private String detail;
}
