package com.zmh.service.impl;

import com.zmh.dao.ItemsDao;
import com.zmh.pojo.Items;
import com.zmh.service.ItemsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Administrator
 * @title: ItemsServiceImpl
 * @projectName maven_day02_1ssm
 * @description: TODO
 * @date 2019/5/69:55
 */
@Service
public class ItemsServiceImpl implements ItemsService {
    @Autowired
    private ItemsDao itemsDao;
    public Items findById(Integer id) {
        return itemsDao.findById(id);
    }
}
