package com.zmh.service;

import com.zmh.pojo.Items;

/**
 * @author Administrator
 * @title: ItemsService
 * @projectName maven_day02_1ssm
 * @description: TODO
 * @date 2019/5/69:52
 */
public interface ItemsService {
    Items findById(Integer id);
}
