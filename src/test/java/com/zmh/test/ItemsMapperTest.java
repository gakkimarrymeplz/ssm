package com.zmh.test;

import com.zmh.dao.ItemsMapper;
import com.zmh.pojo.Items;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author Administrator
 * @title: ItemsMapperTest
 * @projectName maven_day02_1ssm
 * @description: TODO
 * @date 2019/5/521:18
 */
public class ItemsMapperTest {

    @Test
    public void test() {
        //获取容器
        ApplicationContext ac = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        ItemsMapper itemsMapper = ac.getBean(ItemsMapper.class);
        Items items = itemsMapper.findById(1);
        System.out.println(items);
    }
}
